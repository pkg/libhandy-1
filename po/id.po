# Indonesian translation for libhandy.
# Copyright (C) 2020 libhandy's COPYRIGHT HOLDER
# This file is distributed under the same license as the libhandy package.
# Andika Triwidada <atriwidada@gnome.org>, 2020, 2022.
# Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2021.
# Rofiquzzaki <babisayabundar@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: libhandy libhandy-1-6\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libhandy/issues\n"
"POT-Creation-Date: 2022-03-23 09:48+0000\n"
"PO-Revision-Date: 2022-04-01 12:14+0700\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-DL-Team: id\n"
"X-DL-Module: libhandy\n"
"X-DL-Branch: libhandy-1-4\n"
"X-DL-Domain: po\n"
"X-DL-State: Translating\n"

#: glade/glade-hdy-carousel.c:160 glade/glade-hdy-header-bar.c:118
#: glade/glade-hdy-leaflet.c:184
#, c-format
msgid "Insert placeholder to %s"
msgstr "Sisipkan placeholder ke %s"

#: glade/glade-hdy-carousel.c:189 glade/glade-hdy-header-bar.c:144
#: glade/glade-hdy-leaflet.c:214
#, c-format
msgid "Remove placeholder from %s"
msgstr "Hapus placeholder dari %s"

#: glade/glade-hdy-header-bar.c:18
msgid "This property does not apply when a custom title is set"
msgstr "Properti ini tidak berlaku ketika judul ubahan diatur"

#: glade/glade-hdy-header-bar.c:289
msgid ""
"The decoration layout does not apply to header bars which do not show window "
"controls"
msgstr ""
"Tata letak dekorasi tidak berlaku untuk bilah tajuk yang tidak menunjukkan "
"kontrol jendela"

#: glade/glade-hdy-leaflet.c:19
msgid "This property only applies when the leaflet is folded"
msgstr "Properti ini hanya berlaku saat selebaran dilipat"

#: glade/glade-hdy-preferences-page.c:160
#, c-format
msgid "Add group to %s"
msgstr "Tambahkan grup ke %s"

#: glade/glade-hdy-preferences-window.c:228
#, c-format
msgid "Add page to %s"
msgstr "Tambahkan halaman ke %s"

#: glade/glade-hdy-search-bar.c:101
msgid "Search bar is already full"
msgstr "Bilah pencarian sudah penuh"

#: glade/glade-hdy-utils.h:13
#, c-format
msgid "Only objects of type %s can be added to objects of type %s."
msgstr "Hanya objek bertipe %s dapat ditambahkan ke objek bertipe %s."

#: src/hdy-action-row.c:371 src/hdy-action-row.c:372 src/hdy-expander-row.c:319
#: src/hdy-expander-row.c:320 src/hdy-preferences-page.c:173
#: src/hdy-preferences-page.c:174 src/hdy-status-page.c:221
msgid "Icon name"
msgstr "Nama ikon"

#: src/hdy-action-row.c:392
msgid "Activatable widget"
msgstr "Widget yang dapat diaktifkan"

#: src/hdy-action-row.c:393
msgid "The widget to be activated when the row is activated"
msgstr "Widget yang akan diaktifkan saat baris diaktifkan"

#: src/hdy-action-row.c:406 src/hdy-action-row.c:407 src/hdy-expander-row.c:290
#: src/hdy-header-bar.c:2123 src/hdy-view-switcher-title.c:279
msgid "Subtitle"
msgstr "Subjudul"

#: src/hdy-action-row.c:423 src/hdy-expander-row.c:305
#: src/hdy-preferences-row.c:129
msgid "Use underline"
msgstr "Gunakan garis bawah"

#: src/hdy-action-row.c:424 src/hdy-expander-row.c:306
#: src/hdy-preferences-row.c:130
msgid ""
"If set, an underline in the text indicates the next character should be used "
"for the mnemonic accelerator key"
msgstr ""
"Jika diatur, garis bawah dalam teks menunjukkan karakter berikutnya harus "
"digunakan untuk tombol akselerator mnemonik"

#: src/hdy-action-row.c:439
msgid "Number of title lines"
msgstr "Cacah baris judul"

#: src/hdy-action-row.c:440
msgid "The desired number of title lines"
msgstr "Cacah baris judul yang diinginkan"

#: src/hdy-action-row.c:457
msgid "Number of subtitle lines"
msgstr "Cacah baris sub judul"

#: src/hdy-action-row.c:458
msgid "The desired number of subtitle lines"
msgstr "Cacah baris sub judul yang diinginkan"

#: src/hdy-carousel-box.c:1091 src/hdy-carousel-box.c:1092
#: src/hdy-carousel.c:601 src/hdy-carousel.c:602 src/hdy-tab-view.c:1121
msgid "Number of pages"
msgstr "Cacah halaman"

#: src/hdy-carousel-box.c:1107 src/hdy-carousel.c:619 src/hdy-header-bar.c:2095
msgid "Position"
msgstr "Posisi"

#: src/hdy-carousel-box.c:1108 src/hdy-carousel.c:620
msgid "Current scrolling position"
msgstr "Posisi gulir saat ini"

#: src/hdy-carousel-box.c:1123 src/hdy-carousel.c:652 src/hdy-header-bar.c:2151
msgid "Spacing"
msgstr "Jarak"

#: src/hdy-carousel-box.c:1124 src/hdy-carousel.c:653
msgid "Spacing between pages"
msgstr "Jarak antar halaman"

#: src/hdy-carousel-box.c:1140 src/hdy-carousel.c:730
msgid "Reveal duration"
msgstr "Durasi pengungkapan"

#: src/hdy-carousel-box.c:1141 src/hdy-carousel.c:731
msgid "Page reveal duration"
msgstr "Durasi pengungkapan halaman"

#: src/hdy-carousel.c:638
msgid "Interactive"
msgstr "Interaktif"

#: src/hdy-carousel.c:639
msgid "Whether the widget can be swiped"
msgstr "Apakah widget dapat digesek"

#: src/hdy-carousel.c:668
msgid "Animation duration"
msgstr "Durasi animasi"

#: src/hdy-carousel.c:669
msgid "Default animation duration"
msgstr "Durasi animasi baku"

#: src/hdy-carousel.c:684 src/hdy-swipe-tracker.c:1113
msgid "Allow mouse drag"
msgstr "Izinkan seret tetikus"

#: src/hdy-carousel.c:685 src/hdy-swipe-tracker.c:1114
msgid "Whether to allow dragging with mouse pointer"
msgstr "Apakah mengizinkan menyeret dengan penunjuk tetikus"

#: src/hdy-carousel.c:700
msgid "Allow scroll wheel"
msgstr "Izinkan roda penggulir"

#: src/hdy-carousel.c:701
msgid "Whether the widget will respond to scroll wheel events"
msgstr "Apakah widget akan merespon kejadian-kejadian pada roda penggulir"

#: src/hdy-carousel.c:716 src/hdy-swipe-tracker.c:1130
msgid "Allow long swipes"
msgstr "Perbolehkan gesek panjang"

#: src/hdy-carousel.c:717
msgid "Whether to allow swiping for more than one page at a time"
msgstr "Apakah memperbolehkan menggesek lebih dari satu halaman sekaligus"

#: src/hdy-carousel-indicator-dots.c:410 src/hdy-carousel-indicator-dots.c:411
#: src/hdy-carousel-indicator-lines.c:408
#: src/hdy-carousel-indicator-lines.c:409
msgid "Carousel"
msgstr "Karusel"

#: src/hdy-clamp.c:436
msgid "Maximum size"
msgstr "Ukuran maksimum"

#: src/hdy-clamp.c:437
msgid "The maximum size allocated to the child"
msgstr "Ukuran maksimum yang dialokasikan untuk anak"

#: src/hdy-clamp.c:463
msgid "Tightening threshold"
msgstr "Ambang batas pengencangan"

#: src/hdy-clamp.c:464
msgid "The size from which the clamp will tighten its grip on the child"
msgstr "Ukuran penjepit akan mengencangkan cengkeramannya pada anak"

#: src/hdy-combo-row.c:420
msgid "Selected index"
msgstr "Indeks yang dipilih"

#: src/hdy-combo-row.c:421
msgid "The index of the selected item"
msgstr "Indeks item yang dipilih"

#: src/hdy-combo-row.c:439
msgid "Use subtitle"
msgstr "Menggunakan sub judul"

#: src/hdy-combo-row.c:440
msgid "Set the current value as the subtitle"
msgstr "Menetapkan nilai saat ini sebagai sub judul"

#: src/hdy-deck.c:953
msgid "Horizontally homogeneous"
msgstr "Homogen secara horizontal"

#: src/hdy-deck.c:954
msgid "Horizontally homogeneous sizing"
msgstr "Ukuran homogen secara horizontal"

#: src/hdy-deck.c:967
msgid "Vertically homogeneous"
msgstr "Homogen secara vertikal"

#: src/hdy-deck.c:968
msgid "Vertically homogeneous sizing"
msgstr "Ukuran homogen secara vertikal"

#: src/hdy-deck.c:986 src/hdy-leaflet.c:1134 src/hdy-squeezer.c:1136
#: src/hdy-stackable-box.c:3091
msgid "Visible child"
msgstr "Anak yang terlihat"

#: src/hdy-deck.c:987
msgid "The widget currently visible"
msgstr "Widget saat ini terlihat"

#: src/hdy-deck.c:1000 src/hdy-leaflet.c:1150 src/hdy-stackable-box.c:3098
msgid "Name of visible child"
msgstr "Nama anak yang terlihat"

#: src/hdy-deck.c:1001
msgid "The name of the widget currently visible"
msgstr "Nama widget yang saat ini terlihat"

#: src/hdy-deck.c:1018 src/hdy-leaflet.c:1168 src/hdy-squeezer.c:1170
#: src/hdy-stackable-box.c:3117
msgid "Transition type"
msgstr "Jenis transisi"

#: src/hdy-deck.c:1019
msgid "The type of animation used to transition between children"
msgstr "Jenis animasi yang digunakan untuk transisi antara anak-anak"

#: src/hdy-deck.c:1032 src/hdy-header-bar.c:2257 src/hdy-squeezer.c:1150
msgid "Transition duration"
msgstr "Durasi transisi"

#: src/hdy-deck.c:1033
msgid "The transition animation duration, in milliseconds"
msgstr "Durasi animasi transisi, dalam milidetik"

#: src/hdy-deck.c:1046 src/hdy-header-bar.c:2271 src/hdy-squeezer.c:1185
msgid "Transition running"
msgstr "Transisi sedang berjalan"

#: src/hdy-deck.c:1047 src/hdy-header-bar.c:2272 src/hdy-squeezer.c:1186
msgid "Whether or not the transition is currently running"
msgstr "Apakah transisi saat ini sedang berjalan atau tidak"

#: src/hdy-deck.c:1061 src/hdy-header-bar.c:2290 src/hdy-leaflet.c:1224
#: src/hdy-squeezer.c:1204 src/hdy-stackable-box.c:3145
msgid "Interpolate size"
msgstr "Ukuran interpolasi"

#: src/hdy-deck.c:1062 src/hdy-header-bar.c:2291 src/hdy-leaflet.c:1225
#: src/hdy-squeezer.c:1205 src/hdy-stackable-box.c:3146
msgid ""
"Whether or not the size should smoothly change when changing between "
"differently sized children"
msgstr ""
"Apakah ukuran harus secara mulus berubah ketika berganti antara anak-anak "
"dengan ukuran berbeda"

#: src/hdy-deck.c:1075 src/hdy-leaflet.c:1238 src/hdy-preferences-window.c:561
#: src/hdy-stackable-box.c:3160
msgid "Can swipe back"
msgstr "Dapat menggesek mundur"

#: src/hdy-deck.c:1076 src/hdy-leaflet.c:1239 src/hdy-stackable-box.c:3161
msgid ""
"Whether or not swipe gesture can be used to switch to the previous child"
msgstr ""
"Apakah gerakan gesek dapat digunakan untuk beralih ke anak sebelumnya atau "
"tidak"

#: src/hdy-deck.c:1089 src/hdy-leaflet.c:1252 src/hdy-stackable-box.c:3175
msgid "Can swipe forward"
msgstr "Dapat menggesek maju"

#: src/hdy-deck.c:1090 src/hdy-leaflet.c:1253 src/hdy-stackable-box.c:3176
msgid "Whether or not swipe gesture can be used to switch to the next child"
msgstr ""
"Apakah gerakan gesek dapat digunakan untuk beralih ke anak berikutnya atau "
"tidak"

#: src/hdy-deck.c:1098 src/hdy-leaflet.c:1261
msgid "Name"
msgstr "Nama"

#: src/hdy-deck.c:1099 src/hdy-leaflet.c:1262
msgid "The name of the child page"
msgstr "Nama halaman anak"

#: src/hdy-expander-row.c:291
msgid "The subtitle for this row"
msgstr "Subjudul untuk baris ini"

#: src/hdy-expander-row.c:333
msgid "Expanded"
msgstr "Dikembangkan"

#: src/hdy-expander-row.c:334
msgid "Whether the row is expanded"
msgstr "Apakah baris diperluas"

#: src/hdy-expander-row.c:347
msgid "Enable expansion"
msgstr "Aktifkan ekspansi"

#: src/hdy-expander-row.c:348
msgid "Whether the expansion is enabled"
msgstr "Apakah ekspansi diaktifkan"

#: src/hdy-expander-row.c:361
msgid "Show enable switch"
msgstr "Tampilkan sakelar aktifkan"

#: src/hdy-expander-row.c:362
msgid "Whether the switch enabling the expansion is visible"
msgstr "Apakah sakelar yang mengaktifkan ekspansi terlihat"

#: src/hdy-fading-label.c:255 src/hdy-fading-label.c:256
msgid "Label"
msgstr "Label"

#: src/hdy-fading-label.c:262 src/hdy-fading-label.c:263
msgid "Align"
msgstr "Perataan"

#: src/hdy-flap.c:1534
msgid "Content"
msgstr "Isi"

#: src/hdy-flap.c:1535
msgid "The content Widget"
msgstr "Widget konten"

#: src/hdy-flap.c:1550
msgid "Flap"
msgstr "Flap"

#: src/hdy-flap.c:1551
msgid "The flap widget"
msgstr "Widget flap"

#: src/hdy-flap.c:1568
msgid "Separator"
msgstr "Pemisah"

#: src/hdy-flap.c:1569
msgid "The separator widget"
msgstr "Widget pemisah"

#: src/hdy-flap.c:1585
msgid "Flap Position"
msgstr "Posisi Flap"

#: src/hdy-flap.c:1586
msgid "The flap position"
msgstr "Posisi flap"

#: src/hdy-flap.c:1600
msgid "Reveal Flap"
msgstr "Ungkap Flap"

#: src/hdy-flap.c:1601
msgid "Whether the flap is revealed"
msgstr "Apakah flap terungkap"

#: src/hdy-flap.c:1614
msgid "Reveal Duration"
msgstr "Ungkapkan Durasi"

#: src/hdy-flap.c:1615
msgid "The reveal transition animation duration, in milliseconds"
msgstr "Durasi animasi transisi pengungkapan, dalam milidetik"

#: src/hdy-flap.c:1632
msgid "Reveal Progress"
msgstr "Ungkapkan Progres"

#: src/hdy-flap.c:1633
msgid "The current reveal transition progress"
msgstr "Progres transisi pengungkapan saat ini"

#: src/hdy-flap.c:1648
msgid "Fold Policy"
msgstr "Kebijakan Lipat"

#: src/hdy-flap.c:1649
msgid "The current fold policy"
msgstr "Kebijakan lipatan saat ini"

#: src/hdy-flap.c:1663
msgid "Fold Duration"
msgstr "Durasi Lipat"

#: src/hdy-flap.c:1664
msgid "The fold transition animation duration, in milliseconds"
msgstr "Durasi animasi transisi lipat, dalam milidetik"

#: src/hdy-flap.c:1680 src/hdy-leaflet.c:1059 src/hdy-stackable-box.c:3036
msgid "Folded"
msgstr "Dilipat"

#: src/hdy-flap.c:1681
msgid "Whether the flap is currently folded"
msgstr "Apakah flap saat ini dilipat"

#: src/hdy-flap.c:1698
msgid "Locked"
msgstr "Terkunci"

#: src/hdy-flap.c:1699
msgid "Whether the flap is locked"
msgstr "Apakah flap terkunci"

#: src/hdy-flap.c:1716
msgid "Transition Type"
msgstr "Jenis Transisi"

#: src/hdy-flap.c:1717
msgid "The type of animation used for reveal and fold transitions"
msgstr "Jenis animasi yang digunakan untuk transisi pengungkapan dan lipatan"

#: src/hdy-flap.c:1735
msgid "Modal"
msgstr "Modal"

#: src/hdy-flap.c:1736
msgid "Whether the flap is modal"
msgstr "Apakah flap adalah modal"

#: src/hdy-flap.c:1752
msgid "Swipe to Open"
msgstr "Gesek untuk Membuka"

#: src/hdy-flap.c:1753
msgid "Whether the flap can be opened with a swipe gesture"
msgstr "Apakah flap dapat dibuka dengan gerakan gesek"

#: src/hdy-flap.c:1769
msgid "Swipe to Close"
msgstr "Gesek untuk Menutup"

#: src/hdy-flap.c:1770
msgid "Whether the flap can be closed with a swipe gesture"
msgstr "Apakah flap dapat ditutup dengan gerakan gesek"

#: src/hdy-header-bar.c:490
msgid "Application menu"
msgstr "Menu aplikasi"

#: src/hdy-header-bar.c:512 src/hdy-window-handle-controller.c:273
msgid "Minimize"
msgstr "Minimalkan"

#: src/hdy-header-bar.c:534 src/hdy-window-handle-controller.c:239
msgid "Restore"
msgstr "Pulihkan"

#: src/hdy-header-bar.c:534 src/hdy-window-handle-controller.c:282
msgid "Maximize"
msgstr "Maksimalkan"

#: src/hdy-header-bar.c:552 src/hdy-window-handle-controller.c:309
msgid "Close"
msgstr "Tutup"

#: src/hdy-header-bar.c:568
msgid "Back"
msgstr "Mundur"

#: src/hdy-header-bar.c:2088
msgid "Pack type"
msgstr "Jenis pak"

#: src/hdy-header-bar.c:2089
msgid ""
"A GtkPackType indicating whether the child is packed with reference to the "
"start or end of the parent"
msgstr ""
"GtkPackType yang menunjukkan apakah anak dikemas dengan referensi ke awal "
"atau akhir orang tua"

#: src/hdy-header-bar.c:2096
msgid "The index of the child in the parent"
msgstr "Indeks anak dalam induk"

#: src/hdy-header-bar.c:2109 src/hdy-preferences-group.c:294
#: src/hdy-preferences-group.c:295 src/hdy-preferences-page.c:187
#: src/hdy-preferences-page.c:188 src/hdy-preferences-row.c:115
#: src/hdy-status-page.c:235 src/hdy-tab-view.c:441
#: src/hdy-view-switcher-title.c:263
msgid "Title"
msgstr "Judul"

#: src/hdy-header-bar.c:2110 src/hdy-view-switcher-title.c:264
msgid "The title to display"
msgstr "Judul yang akan ditampilkan"

#: src/hdy-header-bar.c:2124 src/hdy-view-switcher-title.c:280
msgid "The subtitle to display"
msgstr "Subjudul yang akan ditampilkan"

#: src/hdy-header-bar.c:2137
msgid "Custom Title"
msgstr "Judul Ubahan"

#: src/hdy-header-bar.c:2138
msgid "Custom title widget to display"
msgstr "Widget judul ubahan yang akan ditampilkan"

#: src/hdy-header-bar.c:2152
msgid "The amount of space between children"
msgstr "Banyaknya ruang antara anak-anak"

#: src/hdy-header-bar.c:2171
msgid "Show decorations"
msgstr "Tampilkan dekorasi"

#: src/hdy-header-bar.c:2172
msgid "Whether to show window decorations"
msgstr "Apakah akan menampilkan dekorasi jendela"

#: src/hdy-header-bar.c:2201
msgid "Decoration Layout"
msgstr "Tata Letak Dekorasi"

#: src/hdy-header-bar.c:2202
msgid "The layout for window decorations"
msgstr "Tata letak untuk dekorasi jendela"

#: src/hdy-header-bar.c:2215
msgid "Decoration Layout Set"
msgstr "Tata Letak Dekorasi Diatur"

#: src/hdy-header-bar.c:2216
msgid "Whether the decoration-layout property has been set"
msgstr "Apakah properti dekorasi-tata letak telah ditetapkan"

#: src/hdy-header-bar.c:2229
msgid "Has Subtitle"
msgstr "Punya Subjudul"

#: src/hdy-header-bar.c:2230
msgid "Whether to reserve space for a subtitle"
msgstr "Apakah akan mencadangkan ruang untuk subjudul"

#: src/hdy-header-bar.c:2243
msgid "Centering policy"
msgstr "Kebijakan memusatkan"

#: src/hdy-header-bar.c:2244
msgid "The policy to horizontally align the center widget"
msgstr "Kebijakan untuk meratakan pusat widget secara horizontal"

#: src/hdy-header-bar.c:2258 src/hdy-squeezer.c:1151
msgid "The animation duration, in milliseconds"
msgstr "Durasi animasi, dalam milidetik"

#: src/hdy-header-group.c:852
msgid "Decorate all"
msgstr "Hiasi semua"

#: src/hdy-header-group.c:853
msgid ""
"Whether the elements of the group should all receive the full decoration"
msgstr "Apakah elemen kelompok semua harus menerima dekorasi penuh"

#: src/hdy-keypad-button.c:225
msgid "Digit"
msgstr "Digit"

#: src/hdy-keypad-button.c:226
msgid "The keypad digit of the button"
msgstr "Digit keypad tombol"

#: src/hdy-keypad-button.c:232
msgid "Symbols"
msgstr "Simbol"

#: src/hdy-keypad-button.c:233
msgid "The keypad symbols of the button. The first symbol is used as the digit"
msgstr "Simbol keypad tombol. Simbol pertama digunakan sebagai digit"

#: src/hdy-keypad-button.c:239
msgid "Show symbols"
msgstr "Tampilkan simbol"

#: src/hdy-keypad-button.c:240
msgid "Whether the second line of symbols should be shown or not"
msgstr "Apakah baris kedua simbol harus ditampilkan atau tidak"

#: src/hdy-keypad.c:258
msgid "Row spacing"
msgstr "Jarak baris"

#: src/hdy-keypad.c:259
msgid "The amount of space between two consecutive rows"
msgstr "Banyaknya ruang antara dua baris berturut-turut"

#: src/hdy-keypad.c:272
msgid "Column spacing"
msgstr "Jarak kolom"

#: src/hdy-keypad.c:273
msgid "The amount of space between two consecutive columns"
msgstr "Banyaknya ruang antara dua kolom berturut-turut"

#: src/hdy-keypad.c:287
msgid "Letters visible"
msgstr "Huruf terlihat"

#: src/hdy-keypad.c:288
msgid "Whether the letters below the digits should be visible"
msgstr "Apakah huruf di bawah digit harus terlihat"

#: src/hdy-keypad.c:304
msgid "Symbols visible"
msgstr "Simbol terlihat"

#: src/hdy-keypad.c:305
msgid "Whether the hash, plus, and asterisk symbols should be visible"
msgstr "Apakah simbol hash, plus, dan tanda bintang harus terlihat"

#: src/hdy-keypad.c:320
msgid "Entry"
msgstr "Entri"

#: src/hdy-keypad.c:321
msgid "The entry widget connected to the keypad"
msgstr "Widget entri yang terhubung ke keypad"

#: src/hdy-keypad.c:334
msgid "End action"
msgstr "Akhiri tindakan"

#: src/hdy-keypad.c:335
msgid "The end action widget"
msgstr "Widget akhiri aksi"

#: src/hdy-keypad.c:348
msgid "Start action"
msgstr "Mulai tindakan"

#: src/hdy-keypad.c:349
msgid "The start action widget"
msgstr "Widget awali aksi"

#: src/hdy-leaflet.c:1060 src/hdy-stackable-box.c:3037
msgid "Whether the widget is folded"
msgstr "Apakah widget dilipat"

#: src/hdy-leaflet.c:1073 src/hdy-stackable-box.c:3048
msgid "Horizontally homogeneous folded"
msgstr "Dilipat secara horizontal homogen"

#: src/hdy-leaflet.c:1074
msgid "Horizontally homogeneous sizing when the leaflet is folded"
msgstr "Ukuran horizontal homogen ketika selebaran dilipat"

#: src/hdy-leaflet.c:1087 src/hdy-stackable-box.c:3060
msgid "Vertically homogeneous folded"
msgstr "Dilipat secara vertikal homogen"

#: src/hdy-leaflet.c:1088
msgid "Vertically homogeneous sizing when the leaflet is folded"
msgstr "Ukuran vertikal homogen ketika selebaran dilipat"

#: src/hdy-leaflet.c:1101 src/hdy-stackable-box.c:3072
msgid "Box horizontally homogeneous"
msgstr "Kotak secara horizontal homogen"

#: src/hdy-leaflet.c:1102
msgid "Horizontally homogeneous sizing when the leaflet is unfolded"
msgstr "Ukuran horizontal homogen ketika selebaran terbuka"

#: src/hdy-leaflet.c:1115 src/hdy-stackable-box.c:3084
msgid "Box vertically homogeneous"
msgstr "Kotak secara vertikal homogen"

#: src/hdy-leaflet.c:1116
msgid "Vertically homogeneous sizing when the leaflet is unfolded"
msgstr "Ukuran vertikal homogen ketika selebaran terbuka"

#: src/hdy-leaflet.c:1135
msgid "The widget currently visible when the leaflet is folded"
msgstr "Widget saat ini terlihat ketika selebaran dilipat"

#: src/hdy-leaflet.c:1151 src/hdy-stackable-box.c:3099
msgid "The name of the widget currently visible when the children are stacked"
msgstr "Nama widget yang saat ini terlihat ketika anak-anak ditumpuk"

#: src/hdy-leaflet.c:1169 src/hdy-stackable-box.c:3118
msgid "The type of animation used to transition between modes and children"
msgstr "Jenis animasi yang digunakan untuk transisi antara mode dan anak-anak"

#: src/hdy-leaflet.c:1182 src/hdy-stackable-box.c:3124
msgid "Mode transition duration"
msgstr "Durasi transisi mode"

#: src/hdy-leaflet.c:1183 src/hdy-stackable-box.c:3125
msgid "The mode transition animation duration, in milliseconds"
msgstr "Durasi animasi transisi mode, dalam milidetik"

#: src/hdy-leaflet.c:1196 src/hdy-stackable-box.c:3131
msgid "Child transition duration"
msgstr "Durasi transisi anak"

#: src/hdy-leaflet.c:1197 src/hdy-stackable-box.c:3132
msgid "The child transition animation duration, in milliseconds"
msgstr "Durasi animasi transisi anak, dalam milidetik"

#: src/hdy-leaflet.c:1210 src/hdy-stackable-box.c:3138
msgid "Child transition running"
msgstr "Transisi anak sedang berjalan"

#: src/hdy-leaflet.c:1211 src/hdy-stackable-box.c:3139
msgid "Whether or not the child transition is currently running"
msgstr "Apakah transisi anak saat ini sedang berjalan atau tidak"

#: src/hdy-leaflet.c:1268
msgid "Navigatable"
msgstr "Dapat dinavigasikan"

#: src/hdy-leaflet.c:1269
msgid "Whether the child can be navigated to"
msgstr "Apakah anak dapat dinavigasi"

#: src/hdy-preferences-group.c:280 src/hdy-preferences-group.c:281
#: src/hdy-status-page.c:249
msgid "Description"
msgstr "Deskripsi"

#: src/hdy-preferences-group.c:308
msgid "Use markup"
msgstr "Gunakan markup"

#: src/hdy-preferences-group.c:309
msgid "Whether to use markup for the title and description"
msgstr "Apakah menggunakan markup pada judul dan deskripsi"

#: src/hdy-preferences-row.c:116
msgid "The title of the preference"
msgstr "Judul preferensi"

#: src/hdy-preferences-window.c:197
msgid "Untitled page"
msgstr "Halaman tanpa judul"

#: src/hdy-preferences-window.c:547
msgid "Search enabled"
msgstr "Pencarian diaktifkan"

#: src/hdy-preferences-window.c:548
msgid "Whether search is enabled"
msgstr "Apakah pencarian diaktifkan"

#: src/hdy-preferences-window.c:562
msgid ""
"Whether or not swipe gesture can be used to switch from a subpage to the "
"preferences"
msgstr ""
"Apakah gerakan gesek dapat digunakan untuk beralih dari subhalaman ke "
"preferensi"

#: src/hdy-preferences-window.ui:9
msgid "Preferences"
msgstr "Preferensi"

#: src/hdy-preferences-window.ui:84
msgid "Search"
msgstr "Cari"

#: src/hdy-preferences-window.ui:167
msgid "No Results Found"
msgstr "Tak Ada Hasil yang Ditemukan"

#: src/hdy-preferences-window.ui:168
msgid "Try a different search."
msgstr "Coba pencarian lain."

#: src/hdy-search-bar.c:451
msgid "Search Mode Enabled"
msgstr "Mode Pencarian Diaktifkan"

#: src/hdy-search-bar.c:452
msgid "Whether the search mode is on and the search bar shown"
msgstr "Apakah mode pencarian aktif dan bilah pencarian ditampilkan"

#: src/hdy-search-bar.c:465
msgid "Show Close Button"
msgstr "Tampilkan Tombol Tutup"

#: src/hdy-search-bar.c:466
msgid "Whether to show the close button in the toolbar"
msgstr "Apakah akan menampilkan tombol tutup di bilah alat"

#: src/hdy-shadow-helper.c:254
msgid "Widget"
msgstr "Widget"

#: src/hdy-shadow-helper.c:255
msgid "The widget the shadow will be drawn for"
msgstr "Widget yang bayangannya akan digambar"

#: src/hdy-squeezer.c:1122
msgid "Homogeneous"
msgstr "Homogen"

#: src/hdy-squeezer.c:1123
msgid "Homogeneous sizing"
msgstr "Ukuran homogen"

#: src/hdy-squeezer.c:1137
msgid "The widget currently visible in the squeezer"
msgstr "Widget saat ini terlihat di squeezer"

#: src/hdy-squeezer.c:1171
msgid "The type of animation used to transition"
msgstr "Jenis animasi yang digunakan untuk transisi"

#: src/hdy-squeezer.c:1226
msgid "X align"
msgstr "Perataan X"

#: src/hdy-squeezer.c:1227
msgid "The horizontal alignment, from 0 (start) to 1 (end)"
msgstr "Perataan horizontal, dari 0 (mulai) ke 1 (akhir)"

#: src/hdy-squeezer.c:1249
msgid "Y align"
msgstr "Perataan Y"

#: src/hdy-squeezer.c:1250
msgid "The vertical alignment, from 0 (top) to 1 (bottom)"
msgstr "Perataan vertikal, dari 0 (atas) ke 1 (bawah)"

#: src/hdy-squeezer.c:1259 src/hdy-swipe-tracker.c:1080
msgid "Enabled"
msgstr "Difungsikan"

#: src/hdy-squeezer.c:1260
msgid ""
"Whether the child can be picked or should be ignored when looking for the "
"child fitting the available size best"
msgstr ""
"Apakah anak dapat dipilih atau harus diabaikan ketika mencari anak yang "
"sesuai dengan ukuran yang tersedia terbaik"

#: src/hdy-stackable-box.c:3049
msgid "Horizontally homogeneous sizing when the widget is folded"
msgstr "Ukuran horizontal homogen ketika widget dilipat"

#: src/hdy-stackable-box.c:3061
msgid "Vertically homogeneous sizing when the widget is folded"
msgstr "Ukuran vertikal homogen ketika widget dilipat"

#: src/hdy-stackable-box.c:3073
msgid "Horizontally homogeneous sizing when the widget is unfolded"
msgstr "Ukuran horizontal homogen ketika widget terbuka"

#: src/hdy-stackable-box.c:3085
msgid "Vertically homogeneous sizing when the widget is unfolded"
msgstr "Ukuran vertikal homogen ketika widget terbuka"

#: src/hdy-stackable-box.c:3092
msgid "The widget currently visible when the widget is folded"
msgstr "Widget yang saat ini terlihat ketika widget dilipat"

#: src/hdy-stackable-box.c:3182 src/hdy-stackable-box.c:3183
msgid "Orientation"
msgstr "Orientasi"

#: src/hdy-status-page.c:222
msgid "The name of the icon to be used"
msgstr "Nama ikon yang akan digunakan"

#: src/hdy-status-page.c:236
msgid "The title to be displayed below the icon"
msgstr "Judul yang akan ditampilkan di bawah ikon"

#: src/hdy-status-page.c:250
msgid "The description to be displayed below the title"
msgstr "Deskripsi yang akan ditampilkan di bawah judul"

#: src/hdy-style-manager.c:583
msgid "Color Scheme"
msgstr "Skema Warna"

#: src/hdy-style-manager.c:584
msgid "The current color scheme"
msgstr "Skema warna saat ini"

#: src/hdy-style-manager.c:606
msgid "System supports color schemes"
msgstr "Sistem mendukung skema warna"

#: src/hdy-style-manager.c:607
msgid "Whether the system supports color schemes"
msgstr "Apakah sistem mendukung skema warna"

#: src/hdy-style-manager.c:623
msgid "Dark"
msgstr "Gelap"

#: src/hdy-style-manager.c:624
msgid "Whether the application is using dark appearance"
msgstr "Apakah aplikasi menggunakan tampilan gelap"

#: src/hdy-style-manager.c:639
msgid "High Contrast"
msgstr "Kontras Tinggi"

#: src/hdy-style-manager.c:640
msgid "Whether the application is using high contrast appearance"
msgstr "Apakah aplikasi menggunakan tampilan kontras tinggi"

#: src/hdy-swipe-tracker.c:1063
msgid "Swipeable"
msgstr "Dapat digesek"

#: src/hdy-swipe-tracker.c:1064
msgid "The swipeable the swipe tracker is attached to"
msgstr "Yang dapat digesek tempat pelacak gesek dilampirkan"

#: src/hdy-swipe-tracker.c:1081
msgid "Whether the swipe tracker processes events"
msgstr "Apakah pelacak gesek memroses peristiwa"

#: src/hdy-swipe-tracker.c:1097
msgid "Reversed"
msgstr "Terbalik"

#: src/hdy-swipe-tracker.c:1098
msgid "Whether swipe direction is reversed"
msgstr "Apakah arah gesek dibalik"

#: src/hdy-swipe-tracker.c:1131
msgid "Whether to allow swiping for more than one snap point at a time"
msgstr ""
"Apakah memperbolehkan menggesek lebih dari satu titik jepretan dalam satu "
"waktu"

#: src/hdy-tab-bar.c:516 src/hdy-tab-box.c:3509 src/hdy-tab-box.c:3510
#: src/hdy-tab.c:906 src/hdy-tab.c:907
msgid "View"
msgstr "Tampilan"

#: src/hdy-tab-bar.c:517
msgid "The view the tab bar controls."
msgstr "Tampilan kontrol bilah tab."

#: src/hdy-tab-bar.c:530
msgid "Start action widget"
msgstr "Mulai widget aksi"

#: src/hdy-tab-bar.c:531
msgid "The widget shown before the tabs"
msgstr "Widget ditampilkan sebelum tab"

#: src/hdy-tab-bar.c:544
msgid "End action widget"
msgstr "Widget akhiri aksi"

#: src/hdy-tab-bar.c:545
msgid "The widget shown after the tabs"
msgstr "Widget ditampilkan setelah tab"

#: src/hdy-tab-bar.c:564
msgid "Autohide"
msgstr "Sembunyi otomatis"

#: src/hdy-tab-bar.c:565
msgid "Whether the tabs automatically hide"
msgstr "Apakah tab secara otomatis bersembunyi"

#: src/hdy-tab-bar.c:580
msgid "Tabs revealed"
msgstr "Tab terungkap"

#: src/hdy-tab-bar.c:581
msgid "Whether the tabs are currently revealed"
msgstr "Apakah tab saat ini terungkap"

#: src/hdy-tab-bar.c:597
msgid "Expand tabs"
msgstr "Perluas tab"

#: src/hdy-tab-bar.c:598
msgid "Whether tabs expand to full width"
msgstr "Apakah tab diperluas ke lebar penuh"

#: src/hdy-tab-bar.c:614 src/hdy-tab.c:948 src/hdy-tab.c:949
msgid "Inverted"
msgstr "Dibalik"

#: src/hdy-tab-bar.c:615
msgid "Whether tabs use inverted layout"
msgstr "Apakah tab menggunakan tata letak terbalik"

#: src/hdy-tab-bar.c:637 src/hdy-tab-bar.c:638
msgid "Extra drag destination targets"
msgstr "Target tujuan seret ekstra"

#: src/hdy-tab-bar.c:654
msgid "Is overflowing"
msgstr "Meluap"

#: src/hdy-tab-bar.c:655
msgid "Whether the tab bar is overflowing"
msgstr "Apakah bilah tab meluap"

#: src/hdy-tab-box.c:3495 src/hdy-tab-box.c:3496 src/hdy-tab.c:913
#: src/hdy-tab.c:914 src/hdy-tab-view.c:424
msgid "Pinned"
msgstr "Disematkan"

#: src/hdy-tab-box.c:3502 src/hdy-tab-box.c:3503
msgid "Tab Bar"
msgstr "Bilah Tab"

#: src/hdy-tab-box.c:3516 src/hdy-tab-box.c:3517
msgid "Adjustment"
msgstr "Penyesuaian"

#: src/hdy-tab-box.c:3523 src/hdy-tab-box.c:3524
msgid "Needs Attention Left"
msgstr "Perlu Perhatian Kiri"

#: src/hdy-tab-box.c:3530 src/hdy-tab-box.c:3531
msgid "Needs Attention Right"
msgstr "Perlu Perhatian Kanan"

#: src/hdy-tab-box.c:3537 src/hdy-tab-box.c:3538
msgid "Resize Frozen"
msgstr "Ubah Ukuran Beku"

#: src/hdy-tab.c:920 src/hdy-tab.c:921
msgid "Dragging"
msgstr "Menyeret"

#: src/hdy-tab.c:927 src/hdy-tab.c:928
msgid "Page"
msgstr "Halaman"

#: src/hdy-tab.c:934 src/hdy-tab.c:935
msgid "Display Width"
msgstr "Lebar Tampilan"

#: src/hdy-tab.c:941 src/hdy-tab.c:942
msgid "Hovering"
msgstr "Melayang"

#: src/hdy-tab-view.c:378
msgid "Child"
msgstr "Anak"

#: src/hdy-tab-view.c:379
msgid "The child of the page"
msgstr "Anak dari halaman"

#: src/hdy-tab-view.c:394
msgid "Parent"
msgstr "Induk"

#: src/hdy-tab-view.c:395
msgid "The parent page of the page"
msgstr "Halaman induk dari halaman"

#: src/hdy-tab-view.c:408
msgid "Selected"
msgstr "Dipilih"

#: src/hdy-tab-view.c:409
msgid "Whether the page is selected"
msgstr "Apakah halaman dipilih"

#: src/hdy-tab-view.c:425
msgid "Whether the page is pinned"
msgstr "Apakah halaman disematkan"

#: src/hdy-tab-view.c:442
msgid "The title of the page"
msgstr "Judul halaman"

#: src/hdy-tab-view.c:460
msgid "Tooltip"
msgstr "Tooltip"

#: src/hdy-tab-view.c:461
msgid "The tooltip of the page"
msgstr "Tooltip halaman"

#: src/hdy-tab-view.c:479
msgid "Icon"
msgstr "Ikon"

#: src/hdy-tab-view.c:480
msgid "The icon of the page"
msgstr "Ikon halaman"

#: src/hdy-tab-view.c:498
msgid "Loading"
msgstr "Memuat"

#: src/hdy-tab-view.c:499
msgid "Whether the page is loading"
msgstr "Apakah halaman sedang dimuat"

#: src/hdy-tab-view.c:523
msgid "Indicator icon"
msgstr "Ikon indikator"

#: src/hdy-tab-view.c:524
msgid "An indicator icon for the page"
msgstr "Ikon indikator untuk halaman"

#: src/hdy-tab-view.c:542
msgid "Indicator activatable"
msgstr "Indikator dapat diaktifkan"

#: src/hdy-tab-view.c:543
msgid "Whether the indicator icon is activatable"
msgstr "Apakah ikon indikator dapat diaktifkan"

#: src/hdy-tab-view.c:560 src/hdy-view-switcher-button.c:232
msgid "Needs attention"
msgstr "Perlu perhatian"

#: src/hdy-tab-view.c:561
msgid "Whether the page needs attention"
msgstr "Apakah halaman perlu diperhatikan"

#: src/hdy-tab-view.c:1122
msgid "The number of pages in the tab view"
msgstr "Cacah halaman dalam tampilan tab"

#: src/hdy-tab-view.c:1137
msgid "Number of pinned pages"
msgstr "Cacah halaman yang disematkan"

#: src/hdy-tab-view.c:1138
msgid "The number of pinned pages in the tab view"
msgstr "Cacah halaman yang disematkan dalam tampilan tab"

#: src/hdy-tab-view.c:1157
msgid "Is transferring page"
msgstr "Sedang mentransfer halaman"

#: src/hdy-tab-view.c:1158
msgid "Whether a page is being transferred"
msgstr "Apakah halaman sedang ditransfer"

#: src/hdy-tab-view.c:1171
msgid "Selected page"
msgstr "Halaman yang dipilih"

#: src/hdy-tab-view.c:1172
msgid "The currently selected page"
msgstr "Halaman yang saat ini dipilih"

#: src/hdy-tab-view.c:1192
msgid "Default icon"
msgstr "Ikon bawaan"

#: src/hdy-tab-view.c:1193
msgid "Default page icon"
msgstr "Ikon halaman bawaan"

#: src/hdy-tab-view.c:1210
msgid "Menu model"
msgstr "Model menu"

#: src/hdy-tab-view.c:1211
msgid "Tab context menu model"
msgstr "Model menu konteks tab"

#: src/hdy-tab-view.c:1247
msgid "Shortcut widget"
msgstr "Widget pintasan"

#: src/hdy-tab-view.c:1248
msgid "Tab shortcut widget"
msgstr "Widget pintasan tab"

#: src/hdy-title-bar.c:329
msgid "Selection mode"
msgstr "Mode seleksi"

#: src/hdy-title-bar.c:330
msgid "Whether or not the title bar is in selection mode"
msgstr "Apakah bilah judul berada dalam mode pilihan atau tidak"

#: src/hdy-value-object.c:208
msgctxt "HdyValueObjectClass"
msgid "Value"
msgstr "Nilai"

#: src/hdy-value-object.c:209
msgctxt "HdyValueObjectClass"
msgid "The contained value"
msgstr "Nilai terkandung"

#: src/hdy-view-switcher-bar.c:179 src/hdy-view-switcher.c:516
#: src/hdy-view-switcher-title.c:232
msgid "Policy"
msgstr "Kebijakan"

#: src/hdy-view-switcher-bar.c:180 src/hdy-view-switcher.c:517
#: src/hdy-view-switcher-title.c:233
msgid "The policy to determine the mode to use"
msgstr "Kebijakan untuk menentukan mode yang akan dipakai"

#: src/hdy-view-switcher-bar.c:193 src/hdy-view-switcher-bar.c:194
#: src/hdy-view-switcher.c:552 src/hdy-view-switcher.c:553
#: src/hdy-view-switcher-title.c:246 src/hdy-view-switcher-title.c:247
msgid "Stack"
msgstr "Tumpukan"

#: src/hdy-view-switcher-bar.c:207
msgid "Reveal"
msgstr "Ungkapkan"

#: src/hdy-view-switcher-bar.c:208
msgid "Whether the view switcher is revealed"
msgstr "Apakah pengalih tampilan terungkap"

#: src/hdy-view-switcher-button.c:201
msgid "Icon Name"
msgstr "Nama Ikon"

#: src/hdy-view-switcher-button.c:202
msgid "Icon name for image"
msgstr "Nama ikon untuk citra"

#: src/hdy-view-switcher-button.c:215
msgid "Icon Size"
msgstr "Ukuran Ikon"

#: src/hdy-view-switcher-button.c:216
msgid "Symbolic size to use for named icon"
msgstr "Ukuran simbolis untuk digunakan bagi ikon bernama"

#: src/hdy-view-switcher-button.c:233
msgid "Hint the view needs attention"
msgstr "Petunjuk tampilan membutuhkan perhatian"

#: src/hdy-view-switcher.c:537
msgid "Narrow ellipsize"
msgstr "Elips sempit"

#: src/hdy-view-switcher.c:538
msgid ""
"The preferred place to ellipsize the string, if the narrow mode label does "
"not have enough room to display the entire string"
msgstr ""
"Tempat yang disukai untuk elips string, jika label mode sempit tidak "
"memiliki cukup ruang untuk menampilkan seluruh string"

#: src/hdy-view-switcher-title.c:300
msgid "View switcher enabled"
msgstr "Pengalih tampilan diaktifkan"

#: src/hdy-view-switcher-title.c:301
msgid "Whether the view switcher is enabled"
msgstr "Apakah pengalih tampilan diaktifkan"

#: src/hdy-view-switcher-title.c:314
msgid "Title visible"
msgstr "Judul terlihat"

#: src/hdy-view-switcher-title.c:315
msgid "Whether the title label is visible"
msgstr "Apakah label judul terlihat"

#: src/hdy-window-handle-controller.c:257
msgid "Move"
msgstr "Pindah"

#: src/hdy-window-handle-controller.c:265
msgid "Resize"
msgstr "Ubah Ukuran"

#: src/hdy-window-handle-controller.c:296
msgid "Always on Top"
msgstr "Selalu di Puncak"
